--drop table if exists users;

create table  users(
  username text primary key
  ,
  bcryptPassword text not null, 
  score INTEGER
);

insert or replace into users(username,bcryptPassword,score) values(
  "admin",
  "$2b$12$9BwyzrkCBioedc6.YLh6xO8jWpHfMeN6hrguMR7qAY7m8CSGrt8Si", -- admin
  0
);
insert or replace into users(username,bcryptPassword,score) values(
  "user",
  "$2b$12$aSC4i0puHmBhEOPdm/ocKOJD4Se3KiajWMznvdx4vN.p./yEWfuee", -- user
  0
);

-- INSERT or replace into users(name,bcryptPassword) VALUES(
--   "ytttt"
--   ,
--   "sssss"
-- );