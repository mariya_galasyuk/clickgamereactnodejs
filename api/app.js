var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
let cors = require("cors");
//var indexRouter = require('./routes/index');
const fs = require('fs');
const sqlite3 = require('sqlite3');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
var app = express();
app.use(cors());
// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'jade');

app.use(logger('dev'));
//app.use(express.json());
//app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
//app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());

app.use(function (req, rsp, next) {
    req.db = app.db;
    next();
});


function createDb(ready) {
  app.db = new sqlite3.Database('sqlite3.db');

  // Make sure tables and initial data exist in the database
  let stmts = fs.readFileSync('schema.sql').toString().split(/;\s*\n/);


  function next(err) {
      if (err) console.warn(err);
      let stmt = stmts.shift();
      if (stmt) app.db.run(stmt, next);
      else if (ready) ready();
  }

  next();
}

createDb();

app.use('/',require('./routes/index.js'));


// catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
