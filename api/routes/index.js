let router = module.exports = require('express').Router();
const bcrypt = require("bcrypt");

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('index', { title: 'Express' });
});

// create a GET route
router.get('/users', (req, rsp) => {
  //res.send(db.prepare('select * from users').all());
  let users = [];
  req.db.all('select * from users', function (err, user) {
    let index = 0;

    user.forEach((row) => {
      users[index] = row;
      index++;
    })
    rsp.status(200).send(users);
    //console.log(users);
  })
  //console.log("here")
});

router.put('/:username/score', (req, rsp) => {
  console.log("req.params.username")
  let username = req.params.username;
console.log(username)
  req.db.get('select * from users where username=?', [username], function (err, user) {
    let sql = 'UPDATE users SET score=? WHERE username=?';
    console.log(username)
    req.db.get(sql, [user.score++, username], function (err) {
      if (err) {
        throw err;
        rsp.status(500).json(err);
        return;
      }
      rsp.status(200).json({ message: 'score added to user' });

    });
  });
})

router.get('/:username/score', (req, rsp) => {
  let username = req.params.username;

  req.db.get('select * from users where username=?', [username], function (err, user) {
    if (user == undefined) {
      rsp.status(200).send("No such user");
      return
    }

    rsp.status(200).send("score of this user is " + user.score);
  });
})


router.post('/register', (req, rsp) => {
  let user = req.body;
  console.log(user)
  req.db.all('select * from users', function (err, users) {

    users.forEach((userSQL) => {

      if (user.username === userSQL.username) {
        console.log("here");
        bcrypt.compare(user.bcryptPassword, userSQL.bcryptPassword, function (err, res) {
          if (err) throw err;
          if (res) {
            console.log(userSQL);
            //res.status(201).json({token: token});
            console.log("user " + user.username + " was logged in")
            rsp.status(200).send("user " + user.username + " was logged in");
          } else {
            rsp.status(403).json({ error: "Invalid credentials" });
          }

        });
        return;
      }
    });
 
  });
createUser(user, req, rsp);
 
});


function createUser(user, req, rsp) {
  console.log("user", user);

  bcrypt.genSalt(10, function (err, salt) {

    bcrypt.hash(user.bcryptPassword, salt, function (err, hash) {

      req.db.get('select * from users where username=? and bcryptPassword =? and score =?', [user.username, user.bcryptPassword, 0], function (err, users) {
        //console.log(user.name)
        if (err) {
          throw err;
        }
        if (users !== undefined) {
          rsp.status(403).json({ error: "Sorry, the same user is already created!" });
          return;
        }
        req.db.run(`INSERT OR REPLACE INTO users VALUES(?,?,?)`, [user.username, hash, 0], function (err, users) {
          if (err) {
            throw ("err");
          }

          rsp.status(201).json(user);

        });
      });


    });
  });
}





